Import-Module $PSScriptRoot\Installer.psm1
$from = $PSScriptRoot
$libraryPath = GetLibraryPath
Write-Host "Library path $libraryPath"
#if version >= 11 then
UnInstallAddIn -name "Diagramy.xla" -to $libraryPath
UnInstallImages -from "$from\images" -to $libraryPath
UninstallTemplates -from "$from\templates"
#if version >= 12 then
UnInstallAddIn -name "DiagramToolBar.xlam" -to $libraryPath
