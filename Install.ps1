Import-Module $PSScriptRoot\Installer.psm1
$from = $PSScriptRoot

$libraryPath = GetLibraryPath
Write-Host "Library path $libraryPath"
#if version >= 11 then
InstallAddIn -name "Diagramy.xla" -from $from -to $libraryPath 
InstallImages -from "$from\images" -to $libraryPath
InstallTemplates -from "$from\templates"
#if version >= 12 then
InstallAddIn -name "DiagramToolBar.xlam" -from $from -to $libraryPath
