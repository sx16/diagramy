#  conan install . -of cmake -c tools.env.virtualenv:powershell=True
from conan import ConanFile
from conan.tools.env import VirtualRunEnv

class Pkg(ConanFile):
    settings = "os", "compiler", "arch", "build_type"
    requires = ["cmake/3.31.0"]

    def generate(self):
        ms = VirtualRunEnv(self)
        ms.generate()
