# Diagramy

Program DIAGRAM powstał dla potrzeb firmy "Zakładu Projektowania i Wdrożeń  PUDLISKI"
do obliczeń hydraulicznych sieci promienistych wody, kanalizacji, wentylacji, gazu
jako nakładka Excela.

Na bazie tego programu  powstał DIAGRAM FAMILIJNY   który obrazuje drzewo genealogiczne
w bardzo zwięzły sposób

## License

GNU GPL v3

## Components

 * AddIns/Diagramy.xla - main extension to create diagram
 * AddIns/DiagramToolBar.xlam - helper extansion to provide ToolBar button
 * CMakeLists.txt - Cmake project file to generate [Nsis project](https://nsis.sourceforge.io/) to create installer
 * Installer.psm1 - powershell module with functions to un/install/enable extensions
