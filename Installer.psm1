function GetExcel {
    return New-Object -ComObject 'Excel.Application'
}

function GetLibraryPath {
    $librarypath = 
    & {
        $excel = GetExcel
        $librarypath = $excel.LibraryPath
        $excel.Quit()
        return $librarypath
    }
    [GC]::Collect()
    return $librarypath
}
function GetTemplatesPath {
    $path = 
    & {
        $excel = GetExcel
        $path = $excel.TemplatesPath
        $excel.Quit()
        return $path
    }
    [GC]::Collect()
    return $path
}
function GetVersion {
    param (
        $excel
    )
    $excel.Version -replace '(\..*)'
}

function SetAddInInstalled {
    param (
        $name,
        $installed
    )
    & {
        $excel = GetExcel
        $excel.AddIns | ForEach-Object {
            if ($_.Name -eq $name) {
                $_.Installed = $installed
            }
        }
        $excel.Quit()
    }
    [GC]::Collect()
}
function InstallAddIn {
    param (
        $name,
        $from,
        $to
    )
    write-Host "Install AddIn: $from\$name  to $to\$name"
    Copy-Item "$from\$name" -Destination "$to"
    SetAddInInstalled $name $true   
}
function UninstallAddIn {
    param (
        $name,
        $to
    )
    write-Host "Uninstall AddIn: $name"
    SetAddInInstalled $name $false
    Remove-Item "$to\$name"
}

function InstallImages {
    param (
        $from,
        $to
    )

    Get-ChildItem $from |
    ForEach-Object {
        Write-Host "Copy file $_ to $to"
        Copy-Item "$from\$_" -Destination "$to"
    }
}

function UninstallImages {
    param (
        $from,
        $to
    )

    Get-ChildItem $from |
    ForEach-Object {
        Write-Host "Delete file $to\$($_.Name)"
        Remove-Item "$to\$($_.Name)"
    }
}

function InstallTemplates {
    param (
        $from
    )

    $to = GetTemplatesPath
    Write-Host "Install templates from $from to $to"
    Get-ChildItem $from |
    ForEach-Object {
        Write-Host "Copy file $_ to $to"
        Copy-Item "$from\$_" -Destination "$to"
    }
}

function UninstallTemplates {
    param (
        $from
    )

    $to = GetTemplatesPath
    Get-ChildItem $from |
    ForEach-Object {
        Write-Host "Delete file $to\$($_.Name)"
        Remove-Item "$to\$($_.Name)"
    }
}
